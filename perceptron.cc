#include <iomanip>  // for setprecisition(), setw()
#include <iostream> // for cout
#include <vector>

#include "perceptron.h"


Perceptron::Perceptron(uint epochs_, float learning_rate_, float recognition_rate_, std::array<float, 2> weights_)
  : epochs{epochs_}, learning_rate{learning_rate_}, recognition_rate{recognition_rate_}, weights{weights_} {}


void Perceptron::train(std::vector<std::array<float, 2>> train_data,
                        std::vector<int> train_output,
                        std::array<std::string, 2> input_designations,
                        std::vector<std::string> output_designations)
{
  std::cout << "epochs (max): " << epochs << std::endl
            << "learning rate: " << learning_rate << std::endl
            << "recognition rate (min): " << recognition_rate << std::endl
            << "starting weights: " << weights[0] << ", " << weights[1] << "\n\n" << std::endl;

  float recognition = 0.0;
  for(uint i = 0; i < epochs && recognition < recognition_rate; ++i)
  {
    std::cout << "epoch: " << i+1 << std::endl;
    int totalerror = 0;

    for (size_t j = 0; j < train_data.size(); ++j)
    {
      int out = align_weights(train_data.at(j), train_output.at(j));
      
      if(train_output.at(j) != out) ++totalerror;
      
      std::string classification = (out == 0) ? output_designations.at(0) : output_designations.at(1);
      std::cout << std::fixed << std::setw(3) << j+1 << ":  " 
                << input_designations[0] <<  ":	" 
                << std::fixed << std::setprecision(1) << train_data.at(j)[0]
                << ",	" << input_designations[1] << ":	" << train_data.at(j)[1]
                << "	->	" << classification << std::endl;
    }

    recognition = calc_recognitionrate(totalerror, train_data.size());
    std::cout << "errors: " << totalerror << std::endl
              << "recognition rate: " << recognition << std::endl
              << "weights: " << std::fixed << std::setprecision(4)
              << weights[0] << ", " << weights[1] << std::endl
              << std::endl;

  }

}


int Perceptron::align_weights(std::array<float, 2> train_data,
                              int train_output)
{
  int out = check_threshold(train_data);
  int error = train_output - out;

  // align weights
  weights[0] += learning_rate * error * train_data[0];
  weights[1] += learning_rate * error * train_data[1];

  return out;
}


int Perceptron::check_threshold(std::array<float, 2> input) const
{
  float sum = 0.0;

  sum += weights[0] * input[0];
  sum += weights[1] * input[1];

  return sum >= 0 ? 1 : 0;
}


float Perceptron::calc_recognitionrate(int totalerror, int train_data_size)
{
  return 100.0 - ((100.0 / train_data_size) * totalerror);
}

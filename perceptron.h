class Perceptron
{

  public:

    // constructs a perceptron object
    // param epochs: max epochs to train
    // param learning_rate: learning rate to train with
    // param recognition_rate: stop training at this rate of successful recognition
    // param weights: starting values of weights
    Perceptron(uint epochs, float learning_rate, float recogition_rate, std::array<float, 2> weights);


    // trains the perceptron
    // iterates through the training data and corrects weights
    // param train_data: an 2D array of the training data
    // param train_output: the expected output for the train_data
    // param input_designations: strings describing input values
    // param output_designations: strings describing output values
    void train(std::vector<std::array<float, 2>> train_data,
                std::vector<int> train_output,
                std::array<std::string, 2> input_designations,
                std::vector<std::string> output_designations);


  private:
  
    uint epochs;
    float learning_rate;
    float recognition_rate;
    std::array<float, 2> weights;


    // align the weights
    // param train_data: current train data points
    // param train_output: expected train output data point
    // return: threshold output, 1 or 0
    int align_weights(std::array<float, 2> train_data,
                        int train_output);


    // checks if weighted input is above threshold, outputs 1 if the sum of
    // the weighted input is above 0
    // param input: an array of the data points data
    // return: 1 or 0
    int check_threshold(std::array<float, 2> input) const;


    // calculates recognition rate
    // param totalerror: the number of total errors in this epoch
    // param train_data_size: the size of the whole training data set
    // return: rate of correct output
    static float calc_recognitionrate(int totalerror, int traindatasize);

};

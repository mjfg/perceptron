#include <array>
#include <getopt.h> // for getopt_long()
#include <iostream> // for cout
#include <string>
#include <tuple>
#include <random>   // for RNG

#include "datareader.h"
#include "perceptron.h"


// default config
uint epochs = 1000;                         // number of epochs to train max
float learning_rate = 0.01;                 // rate to learn with
float recognition_rate = 100.0;             // stop training once rate of recognition reached
std::array<float, 2> weights = {0.0, 0.0};   // weights to start training with
std::string filename = "iris.data";



void print_help(char** argv)
{
  std::cout << "how to use:\n"
            << argv[0] << " [options]\n"
            << "possible options:\n"
            << "-e, --epochs <number of epochs to run max>\n"
            << "-f, --file <filename to read training data from>\n"
            << "-l, --learning-rate <learning rate>\n"
            << "-r, --recognition-rate <recognition rate to be sufficient>\n"
            << "-w, --random-weights  start with random weights\n"
            << "-1, --weight1 <starting value for first weight>\n"
            << "-2, --weight2 <starting value for second weight>\n"
            << "-h, --help  show this help\n";
  
  exit(1);
}



void process_args(int argc, char** argv)
{
  const char* const short_opts = "e:f:l:r:w:1:2:h";
  const option long_opts[] = {
    {"epochs", required_argument, nullptr, 'e'},
    {"file", required_argument, nullptr, 'f'},
    {"learning-rate", required_argument, nullptr, 'l'},
    {"recognition-rate", required_argument, nullptr, 'r'},
    {"random-weights", no_argument, nullptr, 'w'},
    {"weight1", required_argument, nullptr, '1'},
    {"weight2", required_argument, nullptr, '2'},
    {"help", no_argument, nullptr, 'h'},
    {nullptr, no_argument, nullptr, 0}
  };

  while (true)
  {
    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

    if (-1 == opt)
      break;

    switch (opt)
    {
      case 'e':
        epochs = std::stoi(optarg);
        break;      
      case 'f':
        filename = optarg;
        break;
      case 'l':
        learning_rate = std::stof(optarg);
        break;
      case 'r':
        recognition_rate = std::stof(optarg);
        break;
      case 'w':
      {
        // init random generator
        std::random_device rd;
        std::mt19937 mt(rd());
        std::uniform_real_distribution<float> dist(-1, 1);
        // set random weights
        weights[0] = dist(mt);           
        weights[1] = dist(mt);
        break;
      }
      case '1':
        weights[0] = std::stof(optarg);
        break;
      case '2':
        weights[1] = std::stof(optarg);
        break;
      case 'h': // -h or --help
      case '?': // Unrecognized option
      default:
        print_help(argv);
        break;
    }
  }
}



int main(int argc, char* argv[])
{
  
  process_args(argc, argv);

  // construct datareader object
  DataReader datareader(filename);
  // read in data from data file
  auto [ train_data, train_output, input_designations, output_designations ] = datareader.read_data();

  // construct perceptron object
  Perceptron perceptron(epochs, learning_rate, recognition_rate, weights);
  // train perceptron
  perceptron.train(train_data, train_output, input_designations, output_designations);


  return 0;
}

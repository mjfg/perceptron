class DataReader
{

  public:

    // constructs a datareader object
    // param file: file to read
    DataReader(std::string filename);

    // read in data
    // return: a tuple containing train_data, train_output, input_designations, output_designations
    std::tuple<std::vector<std::array<float, 2>>,
               std::vector<int>,
               std::array<std::string, 2>,
               std::vector<std::string>>
               read_data() const;


  private:

    std::string filename;

    // split strings
    // param strToSplit: string to split
    // param delimeter: delimeter to split string at
    // return: a vector of strings containing the parts of the splitted input string
    static std::vector<std::string> split(std::string strToSplit, char delimeter);

};

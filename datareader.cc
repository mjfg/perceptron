#include <algorithm>  // for find()
#include <fstream>    // for file handling
#include <sstream>    // for stringstream in split()
#include <stdexcept>  // for exception handling
#include <tuple>
#include <vector>

#include "datareader.h"

DataReader::DataReader(std::string filename_)
  : filename{filename_} {}


std::vector<std::string> DataReader::split(std::string strToSplit, char delimeter)
{
  std::stringstream ss(strToSplit);
  std::string item;
  std::vector<std::string> splittedStrings;
  while(std::getline(ss, item, delimeter))
  {
     splittedStrings.push_back(item);
  }
  
  return splittedStrings;
}


std::tuple<std::vector<std::array<float, 2>>,
           std::vector<int>,
           std::array<std::string, 2>,
           std::vector<std::string>>
           DataReader::read_data() const
{
  std::vector<std::array<float, 2>> train_data;
  std::vector<int> train_output;
  std::array<std::string, 2> input_designations;
  std::vector<std::string> output_designations;

  // read data file
  std::ifstream file(filename);
  if(!file.good())
  {
    throw std::invalid_argument("training data file does not exist or is not accessible!");
  }
  std::string str;

  bool first_line = true;

  while(std::getline(file, str))
  {
    if(str.size() > 0)
    {
      std::vector<std::string> line_splitted = split(str, 44);

      // get input designations from first line
      if(first_line)
      {
        input_designations[0] = line_splitted.at(0);
        input_designations[1] = line_splitted.at(1);
        first_line = false;
        continue;
      }

      // fill train_data
      float v1 = std::stof(line_splitted.at(0));
      float v2 = std::stof(line_splitted.at(1));
      std::array<float, 2> value_pair = {v1, v2};
      train_data.push_back(value_pair);

      // output designations
      std::string designation = line_splitted.at(2);
      if(output_designations.empty())
      {
        output_designations.push_back(designation);
      }
      else
      {
        if(std::find(output_designations.begin(), output_designations.end(), designation) == output_designations.end())
        {
          output_designations.push_back(designation);
          if(output_designations.size() > 2){
              throw std::invalid_argument("Too many outputs for a binary classifier!");
          }
        }
      }

      // fill output data
      std::vector<std::string>::iterator it = std::find(output_designations.begin(), output_designations.end(), designation);
      int index = std::distance(output_designations.begin(), it);
      train_output.push_back(index);
    }
  }

  std::tuple<std::vector<std::array<float, 2>>,
             std::vector<int>,
             std::array<std::string, 2>,
             std::vector<std::string>>
             return_tuple = std::make_tuple(train_data, train_output, input_designations, output_designations);

  return return_tuple;
}

An implementation of Rosenblatt's perceptron, a simple machine learning algorithm for supervised learning of binary classifiers.


Compilation:  
`g++ -Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic -std=c++17 -o perceptron main.cc perceptron.cc datareader.cc`


Usage:  
`./perceptron [options]`

Options:  
`-e, --epochs <number of epochs to run max>`  
`-f, --file <filename to read training data from>`  
`-l, --learning-rate <learning rate>`  
`-r, --recognition-rate <recognition rate to be sufficient>`  
`-w, --random-weights  start with random weights`  
`-1, --weight1 <starting value for first weight>`  
`-2, --weight2 <starting value for second weight>`  
`-h, --help  show this help`  

If no filename is given the default file `iris.data` is used.

Supply own training data in a text file in the following format:

```
<input_designation 1>, <input_designation 2>
<datapoint 1>, <datapoint 2>, <expected output>
<datapoint 1>, <datapoint 2>, <expected output>
[...]
```
See data file `iris.data` as example.

Example training data is from the iris data set: https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data
